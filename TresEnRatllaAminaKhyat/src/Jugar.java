import java.util.ArrayList;
import java.util.Scanner;

public class Jugar {
	static Scanner reader = new Scanner(System.in);

	public static boolean jugadorsDefinits(ArrayList<String> nomsJugadors) {
		return nomsJugadors.size() >= 2;
	}

	public static void inicialitzarTauler(char[][] tauler) {
		for (int i = 0; i < tauler.length; i++) {
			for (int j = 0; j < tauler[i].length; j++) {
				tauler[i][j] = ' ';
			}
		}
	}

	public static void mostrarTauler(char[][] tauler) {
		for (int i = 0; i < tauler.length; i++) {
			for (int j = 0; j < tauler[i].length; j++) {
				System.out.print(" " + tauler[i][j] + " ");
				if (j != tauler[i].length - 1) {
					System.out.print("|");
				}
			}
			System.out.println();
			if (i != tauler[i].length - 1) {
				System.out.println("-----------");
			}
		}
		System.out.println();
	}

	public static void jugarPartida(char[][] tauler, int posicio1, int posicio2, ArrayList<String> nomsJugadors,
			ArrayList<Integer> partidesGuanyades, ArrayList<Integer> partidesPerdudes) {

		boolean torn1 = true;
		boolean guanyat = false;
		boolean posicionsLliures = true;

		while (!guanyat && posicionsLliures) {
			System.out.println();
			mostrarTauler(tauler);
			if (torn1) {
				guanyat = jugarAmbPosicioTauler(tauler, posicio1, nomsJugadors, 'X');
				if (!guanyat) {
					torn1 = false;
				}
			} else {
				guanyat = jugarAmbPosicioTauler(tauler, posicio2, nomsJugadors, 'O');
				if (!guanyat) {
					torn1 = true;
				}
			}

			posicionsLliures = comprovarPosicionsLliures(tauler);
		}
		if (!posicionsLliures && !guanyat) {
			mostrarTauler(tauler);
			System.out.println("Empat! no ha guanyat ningú");
		}

		if (guanyat) {
			if (torn1) {
				mostrarTauler(tauler);
				System.out.println("Felicitats, " + nomsJugadors.get(posicio1) + ", has guanyat la partida!");
				int partides = partidesGuanyades.get(posicio1) + 1;
				partidesGuanyades.set(posicio1, partides);
				int perdudes = partidesPerdudes.get(posicio2) + 1;
				partidesPerdudes.set(posicio2, perdudes);
			} else {
				mostrarTauler(tauler);
				System.out.println("Felicitats, " + nomsJugadors.get(posicio2) + ", has guanyat la partida!");
				int partides = partidesGuanyades.get(posicio2) + 1;
				partidesGuanyades.set(posicio2, partides);
				int perdudes = partidesPerdudes.get(posicio1) + 1;
				partidesPerdudes.set(posicio1, perdudes);
			}
		}
	}

	public static boolean comprovarPosicionsLliures(char[][] tauler) {
		for (int i = 0; i < tauler.length; i++) {
			for (int j = 0; j < tauler[i].length; j++) {
				if (tauler[i][j] == ' ') {
					return true;
				}
			}
		}
		return false;
	}

	public static boolean partidaGuanyada(char[][] tauler, int fila, int columna, char fitxa) {
		return comprovarTresEnFila(tauler, fila, fitxa) || comprovarTresEnColumna(tauler, columna, fitxa)
				|| comprovarTresEnDiagonal(tauler, columna, fila, fitxa)
				|| comprovarTresEnContraDiagonal(tauler, columna, fila, fitxa);
	}

	public static boolean comprovarTresEnFila(char[][] tauler, int fila, char fitxa) {
		return (tauler[fila][0] == fitxa && tauler[fila][1] == fitxa && tauler[fila][2] == fitxa);
	}

	public static boolean comprovarTresEnColumna(char[][] tauler, int columna, char fitxa) {
		return (tauler[0][columna] == fitxa && tauler[1][columna] == fitxa && tauler[2][columna] == fitxa);
	}

	public static boolean comprovarTresEnDiagonal(char[][] tauler, int columna, int fila, char fitxa) {
		return (fila == columna && tauler[0][0] == fitxa && tauler[1][1] == fitxa && tauler[2][2] == fitxa);
	}

	public static boolean comprovarTresEnContraDiagonal(char[][] tauler, int columna, int fila, char fitxa) {
		return (fila + columna == 2 && tauler[0][2] == fitxa && tauler[1][1] == fitxa && tauler[2][0] == fitxa);
	}

	public static boolean jugarAmbPosicioTauler(char[][] tauler, int posicioJugador, ArrayList<String> nomsJugadors,
			char fitxa) {
		boolean posicioValida = false;
		System.out.println("És el teu torn " + nomsJugadors.get(posicioJugador));
		while (!posicioValida) {
			System.out.println("En quina fila vols posar la teva fitxa?");
			int fila = llegirFilaColumna();
			System.out.println("En quina columna vols posar la teva fitxa?");
			int columna = llegirFilaColumna();

			if (tauler[fila - 1][columna - 1] == ' ') {
				posicioValida = true;
				tauler[fila - 1][columna - 1] = fitxa;

				return partidaGuanyada(tauler, fila-1, columna-1, fitxa);

			} else {
				System.out.println("Aquesta posició està marcada. Prova una altra.");
			}

		}
		return false;
	}

	public static int llegirFilaColumna() {
		boolean correcte = false;
		while (!correcte) {
			try {
				int filaColumna = reader.nextInt();
				if (filaColumna > 3 || filaColumna < 1) {
					System.out.println("Número no correcte! recorda que es un tauler 3x3. Torna a introduir-lo");
				} else {
					return filaColumna;
				}
			} catch (Exception e) {
				System.out.println("Atenció! Únicament es permet insertar números enters. ");
				reader.nextLine();
			}
		}
		return -1;

	}

}
