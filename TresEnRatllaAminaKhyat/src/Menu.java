import java.util.Scanner;

public class Menu {		
	
	static Scanner reader = new Scanner(System.in);
	
	public static void mostrar() {
		System.out.println("Escull una opció: ");
		System.out.println("1. Mostrar ajuda");
		System.out.println("2. Definir Jugador");
		System.out.println("3. Jugar partida");
		System.out.println("4. Veure jugadors");
		System.out.println("0. Sortir");
	}
	
	public static void mostraErrorJugadorNoDefinit() {
		System.out.println("No es pot realitzar aquesta opció sense haver definit els jugadors prèviament");
	}
	
	public static int llegirOpcio() {
		int op = -1;
		System.out.println("Tria una opció");
		while (op > 4 || op < 0) {
			try {
				op = reader.nextInt();
				if (op > 4 || op < 0) {
					System.out.println("Opció no vàlida. Introdueix una altra.");
				}
			} catch (Exception e) {
				System.out.println("Atenció! Únicament es permet insertar números enters. ");
				reader.nextLine();
			}
		}
		return op;
	}

}
