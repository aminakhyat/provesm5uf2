import java.util.ArrayList;

public class Programa {
	public static void main(String[] args) {
		int opcio;
		ArrayList<String> nomsJugadors = new ArrayList<>();
		ArrayList<Integer> partidesGuanyades = new ArrayList<>();
		ArrayList<Integer> partidesPerdudes = new ArrayList<>();
		char [][] tauler = new char [3][3];
		

		do {
			Menu.mostrar();
			opcio = Menu.llegirOpcio();
			switch (opcio) {
			case 1:
				Ajuda.mostraInstruccions();
				break;
			case 2:
				Jugador.definir(nomsJugadors, partidesGuanyades, partidesPerdudes);
		
				break;
			case 3:
				if(Jugar.jugadorsDefinits(nomsJugadors)) {
					int posicio1 = Jugador.llegirJugador1(nomsJugadors);
					int posicio2 = Jugador.llegirJugador2(nomsJugadors, posicio1);
					Jugar.inicialitzarTauler(tauler);
					Jugar.jugarPartida(tauler, posicio1, posicio2, nomsJugadors, partidesGuanyades, partidesPerdudes);
				} else {
					System.out.println("Hi ha menys de 2 jugadors definits.");
				}
				
				break;
			case 4:
				if(Jugar.jugadorsDefinits(nomsJugadors)) {
					Jugador.mostra(nomsJugadors, partidesGuanyades, partidesPerdudes);
				} else {
					System.out.println("Hi ha menys de 2 jugadors definits");
				}
				
				break;
			case 0:
				System.out.println("Sortim del programa");
				break;

			}

		} while (opcio != 0);
	}
}
