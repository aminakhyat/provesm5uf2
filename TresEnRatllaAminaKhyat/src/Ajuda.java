public class Ajuda {

	public static void mostraInstruccions() {
		System.out.println("****  Instrucciones 3 en ratlla  ******");
		System.out.println("----------------------------------------------");
		System.out.println(
				"En aquest joc participen dos jugadors representats al tauler de joc 3x3 per O y X.");
		System.out.println(
				"Cada jugador, al seu torn, marca amb el seu símbol la casella que vol ocupar.");
		System.out.println(
				"Un jugador guanya si aconsegueix tenir una línia de tres dels seus símbols: la línia pot ser horitzontal, vertical o diagonal.");
		System.out.println("Mucha suerte y a jugar....");
		System.out.println("----------------------------------------------");
	}	
}
