import java.util.ArrayList;
import java.util.Scanner;

public class Jugador {
	static Scanner reader = new Scanner(System.in);

	public static void definir(ArrayList<String> nomsJugadors, ArrayList<Integer> partidesGuanyades, ArrayList<Integer> partidesPerdudes) {
		System.out.println("Escriu el nom del jugador/a: ");
		String nom = reader.next();
		nom = nom.toLowerCase();
		if (existeix(nomsJugadors, nom)) {
			System.out.println("Aquest nom d'usuari ja existeix.");
		} else {
			nomsJugadors.add(nom);
			partidesGuanyades.add(0);
			partidesPerdudes.add(0);
		}
	}

	public static boolean existeix(ArrayList<String> nomsJugadors, String nom) {
			if (nomsJugadors.indexOf(nom)!= -1) {
				return true;
			}
		
		return false;
	}

	public static void mostra(ArrayList<String> nomsJugadors, ArrayList<Integer> partidesGuanyades, ArrayList<Integer> partidesPerdudes ) {

		System.out.println("==========================");
		for (int i =0; i <nomsJugadors.size(); i++) {
			System.out.println("NOM DEL JUGADOR/A: " + nomsJugadors.get(i));
			System.out.println("PARTIDES GUANYADES: " + partidesGuanyades.get(i));
			System.out.println("PARTIDES PERDUDES: " + partidesPerdudes.get(i));
			System.out.println("==========================");
		}
	}

	public static int llegirJugador1(ArrayList<String> nomsJugadors) {
		boolean valid = false;
		System.out.println("Qui és el/la jugador/a número 1? Escull un dels definits: " + nomsJugadors);
		while (!valid) {
			String nom = reader.next();
			if (existeix(nomsJugadors, nom)) {
				System.out.println("Jugador/a número 1 preparat/da per la partida!");
				valid = true;
				return nomsJugadors.indexOf(nom);
			} else {
				System.out.println("Aquest jugador/a no està definit/da! Introdueix un nom de la llista.");
			}
		}
		return -1;
	}

	public static int llegirJugador2(ArrayList<String> nomsJugadors, int posicio1) {
		boolean valid = false;
		System.out.println("Qui és el/la jugador/a número 2? Escull un dels definits: " + nomsJugadors);
		while (!valid) {
			String nom = reader.next();
			if (existeix(nomsJugadors, nom)) {
				if (nomsJugadors.indexOf(nom) != posicio1) {
					System.out.println("Jugador/a número 2 preparat/da per la partida!");
					valid = true;
					return nomsJugadors.indexOf(nom);
				} else {
					System.out.println("El/la jugador/a 2 no pot ser el/la mateix/a que el/la jugador/a 1");
				}

			} else {
				System.out.println("Aquest jugador/a no està definit/da! Introdueix un nom de la llista.");
			}
		}
		return -1;
	}
}
